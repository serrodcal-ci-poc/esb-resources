# Project Title

[![pipeline status](https://gitlab.com/serrodcal-ci-poc/esb-resources/badges/master/pipeline.svg)](https://gitlab.com/serrodcal-ci-poc/esb-resources/commits/master)

ESB project and CI example.

## Getting Started

### Prerequisites

Install Docker, Maven and Java 8 in your local. After that, clone this repo.

### Installing

_Under construction_.

## Running the tests

_Under construction_.

## Deployment

_Under construction_.

## Built With

* [Docker](http://www.dropwizard.io/1.2.2/docs/) - Container technology
* [WSO2 ESB](http://www.dropwizard.io/1.2.2/docs/) - ESB Server
* [Maven](https://maven.apache.org/) - Dependency Management
* [Gitlab CI](https://docs.gitlab.com/ee/ci/) - Gitlab CI

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Sergio Rodríguez Calvo** - *Development* - [serrodcal](https://github.com/serrodcal)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
